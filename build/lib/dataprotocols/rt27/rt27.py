from dataprotocols import BaseProtocol


class RT27(BaseProtocol):
    """
    Protocol Doc's
    https://www.trimble.com/OEM_ReceiverHelp/v4.91/en/Default.html#ICD_Pkt_Command64hAPPFILE.html
    https://www.trimble.com/OEM_ReceiverHelp/v4.91/en/ICD_Command64h_AppFile_Output.html
    """
    tipo = "RT27"


